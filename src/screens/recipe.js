import React, { useRef, useLayoutEffect } from "react";
import { StyleSheet, View, TouchableHighlight, Image, ScrollView } from "react-native";

import CarouselHeader from "../components/carouselHeader"
import BodyRecipe from "../components/bodyRecipe"

export default function Recipe({ navigation, route}) {
  const { item } = route.params;
  console.disableYellowBox = true;
  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <TouchableHighlight 
          underlayColor=""
          style={styles.iconeMenu}
          onPress={() => navigation.goBack()}
          >
          <Image 
            style={styles.iconBackArroy}
            source={require('../assets/icons/backArrow.png')}
            />
        </TouchableHighlight> 
      ),
    });
  }, [navigation]);

  return (
    <ScrollView style={styles.container}>
        <CarouselHeader item={item}/>
      <View style={styles.bodyHeader}>
        <BodyRecipe item={item} navigation={navigation}/>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
      backgroundColor: "#fafafa",
  },
  bodyHeader: {
    flex: 2,
  },
  iconeMenu: {
    height: 30,
    width: 35,
    borderRadius: 100,
    backgroundColor: "#fafafa",
    justifyContent: 'center',
    marginLeft: 10,
    alignItems: 'center'
  },
  iconBackArroy: {
    height: 20,
    width: 20,
    padding: 3,
  }
});
